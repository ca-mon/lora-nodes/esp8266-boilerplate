// -----------------------------------------------------------------------------
// MQTT
// -----------------------------------------------------------------------------
#include <ESP8266WiFi.h>
#include <vector>
#include <Ticker.h>
#include "headers/index.h"
#include "headers/main.h"

AsyncMqttClient _mqtt;

Ticker _mqttReconnectTimer;    // Ticker mqtt object

bool _mqtt_enabled = MQTT_ENABLED;
bool _mqtt_forward;
String _mqtt_pass;
String _mqtt_will;
String _mqtt_clientid;
unsigned long _mqtt_reconnect_delay = MQTT_RECONNECT_DELAY_MIN;
unsigned long _mqtt_connected_at = 0;
unsigned long _mqtt_disconnected_at = 0;
std::vector<mqtt_callback_f> _mqtt_callbacks;


void _mqttConnect() {

    // Do not connect if disabled
    if (!_mqtt_enabled) return;

    // Do not connect if already connected
    if (_mqtt.connected()) return;

    if (millis() - _mqtt_disconnected_at < _mqtt_reconnect_delay) return;

    // Increase the reconnect delay
    _mqtt_reconnect_delay += MQTT_RECONNECT_DELAY_STEP;
    if (_mqtt_reconnect_delay > MQTT_RECONNECT_DELAY_MAX) {
        _mqtt_reconnect_delay = MQTT_RECONNECT_DELAY_MAX;
    }

    ledcommand(GREEN, 0.2);

    //May use of a dictionnary to store parameters
    //String r = CLIENTID;
    //const char t[12] = { CLIENTID[0], r[1], r[3], r[4], r[6], r[7], r[9], r[10], r[12], r[13], r[15], r[16] };
    //_mqtt.setWill(MQTT_LAST_WILL_TOPIC, 0, false, t);
    char *host = getServerIP();
    _mqtt.setServer(host, MQTT_PORT);
    _mqtt.setCredentials(MQTT_USERNAME, MQTT_PASSWORD);


    //_mqtt.setClientId(_mqtt_clientid);
    //_mqtt.setKeepAlive(_mqtt_keepalive);
    //_mqtt.setCleanSession(false);

    _mqtt.connect();
}

// -----------------------------------------------------------------------------
// MQTT Callbacks
// -----------------------------------------------------------------------------

void _mqttCallback(unsigned int type, const char *topic, const char *payload) {

    if (type == MQTT_CONNECT_EVENT) {

    }
    if (type == MQTT_MESSAGE_EVENT) {
        if (strcmp(topic, MQTT_RESET_TOPIC) == 0) //Check if the received topic is the relay topic
        {
            deferredReset(300, CUSTOM_RESET_HARDWARE);
        }
    }
}

void _mqttOnConnect(bool sessionPresent) {
    ledcommand(GREEN, 0);
    // Clean subscriptions
    mqttUnsubscribe("#");
    //Publish ClientId at Qos 2
    _mqtt.publish(MQTT_ClientID_TOPIC, 2, false,
                  (CLIENTID + DELIMITER + WiFi.localIP().toString() + DELIMITER + getSocketName() + DELIMITER +
                   THIS_VERSION).c_str());
    _mqtt.publish(MQTT_ClientID_IP_TOPIC, 2, false, (CLIENTID + DELIMITER + WiFi.localIP().toString()).c_str());
    _mqtt.publish(MQTT_ClientID_SOCKETNAME_TOPIC, 2, false, (CLIENTID + DELIMITER + getSocketName()).c_str());
    _mqtt.publish(MQTT_ClientID_VERSION_TOPIC, 2, false, (CLIENTID + DELIMITER + THIS_VERSION).c_str());
    // Send connect event to subscribers
    for (auto &_mqtt_callback : _mqtt_callbacks) {
        _mqtt_callback(MQTT_CONNECT_EVENT, NULL, NULL);
    }

}

//Function called when MQTT connexion disconnect
void _mqttOnDisconnect(AsyncMqttClientDisconnectReason reason) {
    _mqtt_disconnected_at = millis();
    Serial.println("mqtt Disconnected");
    // Send disconnect event to subscribers
    for (auto &_mqtt_callback : _mqtt_callbacks) {
        _mqtt_callback(MQTT_DISCONNECT_EVENT, NULL, NULL);
    }

    if (WiFi.isConnected()) {
        //mqttReconnectTimer.once(2, _mqttConnect);
    }
}

void _mqttOnMessage(char *topic, char *payload, unsigned int len) {

    if (len == 0) return;

    char message[len + 1];
    strlcpy(message, (char *) payload, len + 1);

    //DEBUG_MSG_P(PSTR("[MQTT] Received %s => %s\n"), topic, message);

    // Send message event to subscribers
    for (auto &_mqtt_callback : _mqtt_callbacks) {
        _mqtt_callback(MQTT_MESSAGE_EVENT, topic, message);
    }

}

bool mqttIsConnected() {
    return _mqtt.connected();
}

void mqttSubscribe(const char *topic, uint8_t qos) {
    _mqtt.subscribe(topic, qos);
}

void mqttUnsubscribe(const char *topic) {
    if (_mqtt.connected() && (strlen(topic) > 0)) {
        unsigned int packetId = _mqtt.unsubscribe(topic);
    }
}

void mqttSend(const char *topic, uint8_t qos, bool retain, const char *payload) {
    if (_mqtt.connected()) {
        _mqtt.publish(topic, qos, retain, payload);
    }
}

void mqttRegister(mqtt_callback_f callback) {
    _mqtt_callbacks.push_back(callback);
}

void mqttSetup() {

    _mqtt.onConnect(_mqttOnConnect);

    _mqtt.onDisconnect(_mqttOnDisconnect);

    _mqtt.onMessage(
            [](char *topic, char *payload, AsyncMqttClientMessageProperties properties, size_t len, size_t index,
               size_t total) {
                _mqttOnMessage(topic, payload, len);
            });

    mqttRegister(_mqttCallback);

    // Main callbacks
    espurnaRegisterLoop(_mqttLoop);
}

void _mqttLoop() {
    if (WiFi.status() != WL_CONNECTED) return;
    _mqttConnect();
}