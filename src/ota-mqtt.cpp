#include <ESP8266httpUpdate.h>
#include "headers/index.h"
#include "headers/main.h"

int updateCount = 1;
int _bupdate = 0;
String _ota_path;

void otamqttSetup() {
    mqttRegister(otaVersionMQTTCallback);
    espurnaRegisterLoop(_otamqttLoop);
}

void otaVersionMQTTCallback(unsigned int type, const char *topic, const char *payload) {

    if (type == MQTT_CONNECT_EVENT) {
        // Send status on connect
        mqttSubscribe(MQTT_OTA_TOPIC, 0);

    }
    if (type == MQTT_MESSAGE_EVENT) {
        if (strcmp(topic, MQTT_OTA_TOPIC) == 0) //Check if the received topic is the ota topic
        {
            if (checkMac(payload)) // Check if the mac received is the device mac
            {

                int iPathSize = ((payload[18]) - 48) * 10 + payload[19] - 48 + 1;
                String test;

                for (int i = 21; i < iPathSize + 21; i++) {
                    test += payload[i];
                }
                //strncpy(test, payload + 21, iPathSize);
                Serial.println(test);
                Serial.println(payload);
                OTA(test);
            }
        }
    }
}

void _otamqttLoop() {
    if (_bupdate == 1) {
        _bupdate = 0;
        doUpdate();
    }
}
WiFiClient client;
void doUpdate() {
    if ((WiFi.status() == WL_CONNECTED)) {

        ESPhttpUpdate.rebootOnUpdate(BOOT_AFTER_UPDATE);
        
        t_httpUpdate_return ret = ESPhttpUpdate.update(client, getServerIP(), OTA_PORT, _ota_path.c_str());
        
        USE_SERIAL.print("ret ");
        USE_SERIAL.println(ret);

        switch (ret) {
            case HTTP_UPDATE_FAILED:
                USE_SERIAL.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", ESPhttpUpdate.getLastError(),
                                  ESPhttpUpdate.getLastErrorString().c_str());
                break;

            case HTTP_UPDATE_NO_UPDATES:
                USE_SERIAL.println("HTTP_UPDATE_NO_UPDATES");
                break;

            case HTTP_UPDATE_OK:
                USE_SERIAL.println("HTTP_UPDATE_OK");
                deferredReset(300, CUSTOM_RESET_OTA);
                break;

            default:
                USE_SERIAL.print("Undefined HTTP_UPDATE Code: ");
                USE_SERIAL.println(ret);
        }
    }
}

void OTA(String vers) {
    /*if (!vers.startsWith("http://") && !vers.startsWith("https://")) {
        //DEBUG_MSG_P(PSTR("[OTA] Incorrect URL specified\n"));
        return;
    }*/

    _ota_path = vers;
    _bupdate = 1;
}

bool checkMac(const char *payload) {
    /*String mac = "";
    for (int i = 0; i <= 16; i++) {
      mac += payload[i];
    }
    mac.trim();*/
    return strstr(payload, CLIENTID.c_str()) != NULL;
}