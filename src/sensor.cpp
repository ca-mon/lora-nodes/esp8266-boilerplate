#include <Wire.h>

#include "headers/index.h"
#include "headers/main.h"

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 5     // Digital pin connected to the DHT sensor
// Feather HUZZAH ESP8266 note: use pins 3, 4, 5, 12, 13 or 14 --
// Pin 15 can work but DHT must be disconnected during program upload.

// Uncomment the type of sensor in use:
#define DHTTYPE    DHT11     // DHT 11
//#define DHTTYPE    DHT22     // DHT 22 (AM2302)
//#define DHTTYPE    DHT21     // DHT 21 (AM2301)

DHT_Unified dht(DHTPIN, DHTTYPE);

//HIH8120 Object
HIH61xx<TwoWire> hih(Wire);

//Powermeter Object
//PowerMeter P1;

TICK _tReadingTemp = 0;
TICK _tReadingTempDHT = 0;
TICK _tReadingPM1 = 0;

bool _bSendTempHumi = false;
bool _bSendPower = false;
int _iTemp, _iHumi;

void _TempsensorMQTT() {
    mqttSend(MQTT_TEMP_TOPIC, 0, false, (CLIENTID + DELIMITER + _iTemp).c_str());
    mqttSend(MQTT_HUMI_TOPIC, 0, false, (CLIENTID + DELIMITER + _iHumi).c_str());
}

void _PowersensorMQTT() {
    //mqttSend(MQTT_TENSION_TOPIC, 0, false, (CLIENTID + DELIMITER + P1._PowerValues.fVoltage).c_str());
    mqttSend(MQTT_CURRENT_TOPIC, 0, false, (CLIENTID + DELIMITER + "0.15").c_str());
    //mqttSend(MQTT_FREQUENCY_TOPIC, 0, false, (CLIENTID + DELIMITER + P1._PowerValues.fFrequency).c_str());

    mqttSend(MQTT_APPARENTE_POWER_TOPIC, 0, false, (CLIENTID + DELIMITER + "155").c_str());
    //mqttSend(MQTT_ACTIVE_POWER_TOPIC, 0, false, (CLIENTID + DELIMITER + P1._PowerValues.fActivePower).c_str());
    //mqttSend(MQTT_REACTIVE_POWER_TOPIC, 0, false, (CLIENTID + DELIMITER + P1._PowerValues.fReactivePower).c_str());
}

void sensorSetup() {
    Wire.begin();
    hih.initialise();
    dht.begin();
    sensor_t sensor;
    dht.temperature().getSensor(&sensor);
    Serial.println(F("------------------------------------"));
    Serial.println(F("Temperature Sensor"));
    Serial.print(F("Sensor Type: "));
    Serial.println(sensor.name);
    Serial.print(F("Driver Ver:  "));
    Serial.println(sensor.version);
    Serial.print(F("Unique ID:   "));
    Serial.println(sensor.sensor_id);
    Serial.print(F("Max Value:   "));
    Serial.print(sensor.max_value);
    Serial.println(F("°C"));
    Serial.print(F("Min Value:   "));
    Serial.print(sensor.min_value);
    Serial.println(F("°C"));
    Serial.print(F("Resolution:  "));
    Serial.print(sensor.resolution);
    Serial.println(F("°C"));
    Serial.println(F("------------------------------------"));
    // Print humidity sensor details.
    dht.humidity().getSensor(&sensor);
    Serial.println(F("Humidity Sensor"));
    Serial.print(F("Sensor Type: "));
    Serial.println(sensor.name);
    Serial.print(F("Driver Ver:  "));
    Serial.println(sensor.version);
    Serial.print(F("Unique ID:   "));
    Serial.println(sensor.sensor_id);
    Serial.print(F("Max Value:   "));
    Serial.print(sensor.max_value);
    Serial.println(F("%"));
    Serial.print(F("Min Value:   "));
    Serial.print(sensor.min_value);
    Serial.println(F("%"));
    Serial.print(F("Resolution:  "));
    Serial.print(sensor.resolution);
    Serial.println(F("%"));
    Serial.println(F("------------------------------------"));
    //P1.setDebugOutput(false);
    //P1.PowerMeterInit(D7, D6);
    //P1.StartEnergyCount();

    _tReadingTemp = millis();
    _tReadingPM1 = millis();
    _tReadingTempDHT = millis();

    espurnaRegisterLoop(_sensorLoop);
}

void _sensorLoop() {
    /////////////////////////////////////
    //Temp/Humi sensor process and timing
    /////////////////////////////////////
    /*  if ((TickGetDiff(millis(), _tReadingTemp) > Read_intervalTemp) && !hih.isSampling()) {
          hih.start();
          _bSendTempHumi = true;
          _tReadingTemp = millis();
      }

      hih.process();

      if (hih.isFinished() && _bSendTempHumi) {
          _iTemp = hih.getAmbientTemp() / 100;
          _iHumi = hih.getRelHumidity() / 100;
          _TempsensorMQTT();
          _bSendTempHumi = false;
      }*/

    if ((TickGetDiff(millis(), _tReadingTempDHT) > 1000)) {
        _tReadingTempDHT = millis();
        sensors_event_t event;
        dht.temperature().getEvent(&event);
        if (isnan(event.temperature)) {
            Serial.println(F("Error reading temperature!"));
        } else {
            // Serial.print(F("Temperature: "));
            // Serial.print(event.temperature);
            // Serial.println(F("°C"));
            _iTemp = event.temperature;
        }
        // Get humidity event and print its value.
        dht.humidity().getEvent(&event);
        if (isnan(event.relative_humidity)) {
            Serial.println(F("Error reading humidity!"));
        } else {
            // Serial.print(F("Humidity: "));
            // Serial.print(event.relative_humidity);
            // Serial.println(F("%"));
            _iHumi = event.relative_humidity;
        }
        _TempsensorMQTT();
    }



    //////////////////////////////////////////////
    //Power Meter sensor sensor process and timing
    //////////////////////////////////////////////
    /* if ((TickGetDiff(millis(), _tReadingPM1) > Read_intervalPM1)) {
          //Serial.println(millis());
          //P1.PowerMeterSend();
          _bSendPower = true;
          _tReadingPM1 = millis();
      }*/

    //P1.ReadPowerValues();

    //Send Power data only if the reading is finished and a new reading has been completed the connection to mqtt is valid and the relay is switched on
    /*  if (_bSendPower && getRelayState()) {
          _PowersensorMQTT();
          _bSendPower = false;
      }*/
}
