// -----------------------------------------------------------------------------
// BUTTON
// -----------------------------------------------------------------------------
#include "headers/index.h"
#include "headers/main.h"
//Global variables for millis and button functions

TICK _tReadingStatus = 0;

void statusMQTT() {
    mqttSend(MQTT_STATUS_PONG, 2, false, (CLIENTID + DELIMITER + STATUS_PONG_GOOD).c_str());
}

void statusMQTTCallback(unsigned int type, const char *topic, const char *payload) {
    if (type == MQTT_CONNECT_EVENT) {
        // Send status on connect
        mqttSubscribe(MQTT_STATUS_PING, 2);
        statusMQTT();
    }
    if (type == MQTT_MESSAGE_EVENT) {
        String mac = "";
        if (strcmp(topic, MQTT_STATUS_PING) == 0) //Check if the received topic is the relay topic
        {
            if (checkMac(payload)) // Check if the mac received is the device mac
            {
                statusMQTT();
            }
        }
    }
}

void statusSetupMQTT() {
    mqttRegister(statusMQTTCallback);
}

void statusSetup() {
    statusSetupMQTT();
    // Main callbacks
    espurnaRegisterLoop(statusLoop);
}

void statusLoop() {
    if ((TickGetDiff(millis(), _tReadingStatus) > 100)) {
        _tReadingStatus = millis();
        // statusMQTT();
    }
}