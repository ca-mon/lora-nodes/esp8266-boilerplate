// -----------------------------------------------------------------------------
// LED
// -----------------------------------------------------------------------------

#include <Adafruit_NeoPixel.h>
#include <Ticker.h>
#include "headers/index.h"

//WS2812 LED object
Adafruit_NeoPixel _led = Adafruit_NeoPixel(NUM_LEDS, RGB_LED_PIN, NEO_GRBW + NEO_KHZ800);

Ticker _LedTimer;

//Global variables for the LED
bool _bBLink = false;
long _xColor;

//Pin for if led is PWM
//int rgbPins[3] = { 12, 14, 13 };

//Function to manage LED state
void ledcommand(long color, float sec) {
    if (sec == 0) {
        _LedTimer.detach();
        //map(color >> 16, 0, 255, 0, 1023)
        /*analogWrite(rgbPins[0], color >> 16);
        analogWrite(rgbPins[1], color >> 8 & 0xFF);
        analogWrite(rgbPins[2], color & 0xFF);*/
        _led.setPixelColor(0, color);
        _led.show();
    } else {
        _xColor = color;
        _LedTimer.attach(sec, _Blinkled);
    }
}

//Function called for blinking the LED
void _Blinkled(void) {
    if (_bBLink) {
        /*analogWrite(rgbPins[0], color >> 16);
        analogWrite(rgbPins[1], color >> 8 & 0xFF);
        analogWrite(rgbPins[2], color & 0xFF);*/
        _led.setPixelColor(0, _xColor);
        _led.show();
    } else {
        _led.setPixelColor(0, OFF);
        _led.show();
    }
    _bBLink = !_bBLink;
}

void ledSetup() {

    /*for (int i = 0; i < 3; i++) {
    pinMode(rgbPins[i], OUTPUT);
}*/

    _led.setBrightness(BRIGHTNESS);
    _led.begin();
    _led.show(); // Initialize all pixels to 'off'
}

void _ledLoop() {
    //void ledState() {
//	switch (bState)
//	{
//	case 0: // When Esp set to ap mode blinking blue
//		ledcommand(BLUE, 0.5);
//		break;
//	case 1: // When Esp connecting to wifi blink Orange
//		ledcommand(ORANGE, 0.5);
//		break;
//	case 2: // When Esp connected to wifi Orange
//		ledcommand(ORANGE, 0);
//		break;
//	case 3: // When Esp connecting to mqtt broker blink Green 
//		//LedTimer.attach(0.5, Blinkled);
//		ledcommand(GREEN, 0);
//		break;
//	case 4: // When Esp connected to mqtt broker Green 
//		ledcommand(GREEN, 0);
//		break;
//	case 5: // When problem blink red fast
//		ledcommand(RED, 0.2);
//		break;
//	default:
//		break;
//	}
//}
    ;
}
