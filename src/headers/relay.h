//
// Created by abocc on 25/07/2020.
//

#ifndef INC_200725_135000_NODEMCUV2_RELAY_H
#define INC_200725_135000_NODEMCUV2_RELAY_H

#endif //INC_200725_135000_NODEMCUV2_RELAY_H


bool getRelayState();

void _relayConfigure();

void _relayBoot();

void relaySwitch();

void relayMQTT();

void relayMQTTCallback(unsigned int type, const char *topic, const char *payload);

void relaySetupMQTT();

void _relayLoop();

void relaySetup();