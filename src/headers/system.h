//
// Created by abocc on 25/07/2020.
//

#ifndef INC_200725_135000_NODEMCUV2_SYSTEM_H
#define INC_200725_135000_NODEMCUV2_SYSTEM_H

#endif //INC_200725_135000_NODEMCUV2_SYSTEM_H

void resetReason(unsigned char reason);

void reset();

void deferredReset(unsigned long delay, unsigned char reason);

bool checkNeedsReset();

void systemSetup();

void systemLoop();