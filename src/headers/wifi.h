//
// Created by abocc on 25/07/2020.
//
#include <WiFiManager.h>            //https://github.com/tzapu/WiFiManager

#ifndef INC_200725_135000_NODEMCUV2_WIFI_H
#define INC_200725_135000_NODEMCUV2_WIFI_H

#endif //INC_200725_135000_NODEMCUV2_WIFI_H

void _onWifiDisconnect(const WiFiEventStationModeDisconnected &event);

void _onAPstarted(WiFiManager *);

void _saveConfigCallback();

void _connectToWifi();

void _onWifiConnect();

char *getServerIP();

String getSocketName();

void wifiReset();

void wifiSetup();

void wifiLoop();