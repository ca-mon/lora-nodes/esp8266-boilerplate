//
// Created by abocc on 25/07/2020.
//

#ifndef INC_200725_135000_NODEMCUV2_OTAUPDATE_H
#define INC_200725_135000_NODEMCUV2_OTAUPDATE_H

#endif //INC_200725_135000_NODEMCUV2_OTAUPDATE_H

#define OTA_MQTT_SUPPORT        1            // No support by default
#define USE_PASSWORD            1               // Insecurity caution! Disabling this will disable password querying completely.

void _otaConfigure();

void _otaLoop();

void _otaFrom(const char *host, unsigned int port, const char *url);

void _otaFrom(String url);

void _otaMQTTCallback(unsigned int type, const char *topic, const char *payload);

void otaSetup();
