#pragma once //preventing a header file from being included multiple times (same as #ifndef FUNCTION_H #define FUNCTION_H #endif)

///////////////#define/////////////////////////////////////////////
#define THIS_VERSION "2.0.0"

#define MS_TO_SEC(a)        (a/1000.0)
#define SEC_TO_HR(a)        (a/3600.0)
#define TickGetDiff(a, b)    (a-b)

//Wifi credentials
#define WIFI_SSID        "SFR-01c8" // "SFR-01c8"//"Livebox-9C06" ////
#define WIFI_PASSWORD    "DZZFKIDFAU4A" // "DZZFKIDFAU4A" //"Q6ddDxGZ7si4eCuCV2"////

//MQTT address and port
#define MQTT_HOST IPAddress(192, 168, 0, 29)
#define MQTT_PORT 1883

//MQTT credentials
#define MQTT_USERNAME "user"
#define MQTT_PASSWORD "pass"

//Payload message delimiter
#define DELIMITER '@'
//Unique client id
#define CLIENTID WiFi.macAddress()
#define MAC_SIZE 16

#ifndef MQTT_SUPPORT
#define MQTT_SUPPORT                1           // MQTT support (22.38Kb async, 12.48Kb sync)
#endif

#ifndef LED_RELAY_STATUS
#define LED_RELAY_STATUS            0            // Led for relay status
#endif

#ifndef DHT22_SENSOR
#define DHT22_SENSOR            0            // Led for relay status
#endif

#ifndef HIH61_SENSOR
#define HIH61_SENSOR            1            // Led for relay status
#endif

//MQTT Topics
#define MQTT_RESET_TOPIC            "ESP/reset/socket"
#define MQTT_ClientID_TOPIC            "ESP/ClientID"
#define MQTT_ClientID_IP_TOPIC            "ESP/ClientID/IP"
#define MQTT_ClientID_SOCKETNAME_TOPIC     "ESP/ClientID/SOCKETNAME"
#define MQTT_ClientID_VERSION_TOPIC            "ESP/ClientID/VERSION"
#define MQTT_LAST_WILL_TOPIC        "ESP/LastWill/socket"
#define MQTT_OTA_TOPIC                "ESP/ota/socket"
#define MQTT_TEMP_TOPIC                "home/sensor/temperature"
#define MQTT_HUMI_TOPIC                "home/sensor/humidity"
#define MQTT_POWER_TOPIC            "home/sensor/apparent_power" //Topic to send power values
#define MQTT_RELAY_TOPIC            "home/cmd/relay" // Topic to commmand the relay ('0' or '1')
#define MQTT_RELAY_STATUT_TOPIC        "home/status/relay" // Topic for the acknolegment of the relay state
#define MQTT_STATUS_PING "ESP/status/ping"
#define MQTT_STATUS_PONG "ESP/status/pong"

#define MQTT_REACTIVE_POWER_TOPIC    "home/sensor/reactive"
#define MQTT_APPARENTE_POWER_TOPIC    "home/sensor/apparente"
#define MQTT_ACTIVE_POWER_TOPIC        "home/sensor/active"
#define MQTT_FACTOR_POWER_TOPIC        "home/sensor/factor"
#define MQTT_TENSION_TOPIC            "home/sensor/tension"
#define MQTT_CURRENT_TOPIC            "home/sensor/current"
#define MQTT_FREQUENCY_TOPIC        "home/sensor/frequency"

// Configure the MQTT payload for ON/OFF
#define RELAY_MQTT_ON    "1"
#define RELAY_MQTT_OFF    "0"

#define STATUS_PONG_GOOD true
#define STATUS_PONG_BAD false

//PowerMeter settings
#define TICK unsigned long

#define Read_intervalPM1    1500   //150ms delay after sending the read frame
#define Read_intervalPM2    60000   //150ms delay after sending the read frame
#define Read_intervalTemp    30000   //150ms delay after sending the read frame

//LED colors HEX
#define BLUE    0x0000ff
#define RED        0xff0000
#define GREEN    0x00ff00
#define ORANGE    0xff6600
#define OFF        0x000000

//LED configuration
#define RGB_LED_PIN 12 //D9 //IO12
#define NUM_LEDS    1
#define BRIGHTNESS    20

//Button configuration
#define BUTTON_PIN    14 //SCK //IO14

//Relay configuration
#define RELAY_OFF            HIGH
#define RELAY_ON            LOW
#define RELAY_PIN            10 //D3 //IO10
#define RELAY_FLOOD_TIME    2000

//------------------------------------------------------------------------------
// RESET
//------------------------------------------------------------------------------
#define CUSTOM_RESET_HARDWARE       1       // Reset from hardware button
#define CUSTOM_RESET_WEB            2       // Reset from web interface
#define CUSTOM_RESET_TERMINAL       3       // Reset from terminal
#define CUSTOM_RESET_MQTT           4       // Reset via MQTT
#define CUSTOM_RESET_RPC            5       // Reset via RPC (HTTP)
#define CUSTOM_RESET_OTA            6       // Reset after successful OTA update
#define CUSTOM_RESET_HTTP           7       // Reset via HTTP GET
#define CUSTOM_RESET_UPGRADE        9       // Reset after update from web interface
#define CUSTOM_RESET_FACTORY        10      // Factory reset from terminal
#define CUSTOM_RESET_MAX            10

//------------------------------------------------------------------------------
// OTA
//------------------------------------------------------------------------------
#define BOOT_AFTER_UPDATE true
#define OTA_PORT 8080

