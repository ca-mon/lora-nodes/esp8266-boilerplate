//
// Created by abocc on 25/07/2020.
//

#ifndef INC_200725_135000_NODEMCUV2_OTA_MQTT_H
#define INC_200725_135000_NODEMCUV2_OTA_MQTT_H

#endif //INC_200725_135000_NODEMCUV2_OTA_MQTT_H

#define USE_SERIAL Serial
#define DEBUG_ESP_PORT Serial

void otamqttSetup();

void otaVersionMQTTCallback(unsigned int type, const char *topic, const char *payload);

void _otamqttLoop();

void doUpdate();

void OTA(String vers);

bool checkMac(const char *payload);