#include<SoftwareSerial.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#ifndef _POWER_METER_
#define _POWER_METER_

#define byte uint8_t

#define MS_TO_SEC(a)        (a/1000.0)
#define SEC_TO_HR(a)        (a/3600.0)
#define TickGetDiff(a, b)    (a-b)

#define ACK 0x06
#define NAK 0x15

#define MCP39F501_BAUD_RATE 4800

#define CURRENT_MULTIPLIER 0.0001
#define VOLTAGE_MULTIPLIER 0.1
#define ACTIVE_POWER_MULTIPLIER 0.01
#define REACTIVE_POWER_MULTIPLIER 0.01
#define APPARENT_POWER_MULTIPLIER 0.01
#define FREQUENCY_MULTIPLIER 0.001
#define POWERFACTOR_MULTIPLIER 0.001

#define APPARENT_POWER_THRESHOLD 1 //1W

#define NUMBER_OF_BYTES_READ 0x1D
#define MCP_START_FRAME 0xA5
#define MCP_READ 0x4E
#define MCP_WRITE 0x4D

class PowerMeter {

public:
    PowerMeter();

    void setDebugOutput(bool debug);

    struct PowerValues {
        float fCurrent;
        float fVoltage;
        float fActivePower;        // P=U*I*cos(PowerFactor)
        float fReactivePower;    // P=U*I*sin(PowerFactor)
        float fApparentPower;    // S=U*I
        float fPowerFactor;        // Phi = P/S
        float fFrequency;
        float fPowerCount;
    };
    struct PowerEvents {
        uint16_t iEventsRegisters;
        bool bOverCurrent;
        bool bOverVoltage;
        bool bUnderVoltage;
        bool bOverPower;
        bool bOverFrequency;
        bool bUnderFrequency;
        bool bOverTemperature;
        bool bUnderTemperature;
        bool bVoltageSag;
        bool bVoltageSurge;
    };

    PowerValues _PowerValues;
    PowerEvents _PowerEvents;

    void PowerMeterInit(byte rxPin, byte txPin);

    void ReadPowerValues(void);

    void PowerMeterSend(void);

    bool isFinished(void);

    bool isSampling(void);

    void Calibration1Sequence(void);

    void Calibration2Sequence(void);

    void StartEnergyCount(void);

    void StopEnergyCount(void);

private:

    enum state_t {
        reading, // Ready to read results
        finished // Results read
    };

    SoftwareSerial *Uart1;// (D7, D6);

    state_t ReadingState = finished;

    byte pmData[50];
    byte pmDataEnergy[50];
    byte pmDataLen = 0;
    byte pmDataLenEnergy = 0;
    byte CalibrationState = 0;

    bool Calibration1 = false;
    bool Calibration2 = false;
    bool ReadCommand = false;
    bool bEnergycount = false;

    bool _debug = true;

    unsigned long TimeLastReading;

    void PowerMeterClearBuffer(void);

    byte PowerMeterGetDataSize();

    void error(byte e);

    void SetRange(void);

    void FrequencyCalibrationCommand(void);

    void GainCalibrationCommand(void);

    void ReactiveCalibrationCommand(void);

    void SaveToFlashCommand(void);

    void DEBUG_WM(String text);
};

#endif