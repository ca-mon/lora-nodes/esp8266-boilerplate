//
// Created by abocc on 25/07/2020.
//

#ifndef INC_200725_135000_NODEMCUV2_MQTT_H
#define INC_200725_135000_NODEMCUV2_MQTT_H

#include <AsyncMqttClient.h>

#endif //INC_200725_135000_NODEMCUV2_MQTT_H

typedef std::function<void(unsigned int, const char *, const char *)> mqtt_callback_f;

// Internal MQTT events
#define MQTT_CONNECT_EVENT          0
#define MQTT_DISCONNECT_EVENT       1
#define MQTT_MESSAGE_EVENT          2
#define MQTT_ENABLED                true
#define MQTT_RECONNECT_DELAY_MIN    20000            // Try to reconnect in 5 seconds upon disconnection
#define MQTT_RECONNECT_DELAY_STEP   20000            // Increase the reconnect delay in 5 seconds after each failed attempt
#define MQTT_RECONNECT_DELAY_MAX    120000          // Set reconnect time to 2 minutes at most

void mqttRegister(mqtt_callback_f callback);

void _mqttOnDisconnect(AsyncMqttClientDisconnectReason reason);

void _mqttConnect();

void _mqttCallback(unsigned int type, const char *topic, const char *payload);

void _mqttOnConnect(bool sessionPresent);

void _mqttOnMessage(char *topic, char *payload, unsigned int len);

bool mqttIsConnected();

void mqttSubscribe(const char *topic, uint8_t qos);

void mqttUnsubscribe(const char *topic);

void mqttSend(const char *topic, uint8_t qos, bool retain, const char *payload);

void mqttRegister(mqtt_callback_f callback);

void mqttSetup();

void _mqttLoop();

