// -----------------------------------------------------------------------------
// RELAY
// -----------------------------------------------------------------------------
#include "headers/index.h"
#include "headers/main.h"

enum state_t {
    Init, // Conversion started, waiting for completion
    Change, // Ready to read results
    Wait // Results read
};

bool _bCurrent_status = RELAY_MQTT_OFF;
bool _bTarget_status = RELAY_MQTT_OFF;

state_t _RelayState = Init;

unsigned long _lFlood_start;

bool getRelayState() {
    return _bCurrent_status;
}

void _relayConfigure() {
    pinMode(RELAY_PIN, OUTPUT);
}

void _relayBoot() {
    digitalWrite(RELAY_PIN, RELAY_OFF);//Set the relay off
    _bCurrent_status = RELAY_MQTT_OFF;
    _bTarget_status = RELAY_MQTT_OFF;
}

void relaySwitch() {
    _bTarget_status = !digitalRead(RELAY_PIN);
}

void relayMQTT() {
    mqttSend(MQTT_RELAY_STATUT_TOPIC, 2, false,
             !digitalRead(RELAY_PIN) ? (CLIENTID + DELIMITER + RELAY_MQTT_ON).c_str() : (CLIENTID + DELIMITER +
                                                                                         RELAY_MQTT_OFF).c_str());
}

void relayMQTTCallback(unsigned int type, const char *topic, const char *payload) {

    if (type == MQTT_CONNECT_EVENT) {
        // Send status on connect
        mqttSubscribe(MQTT_RELAY_TOPIC, 2);
        relayMQTT();
    }
    if (type == MQTT_MESSAGE_EVENT) {
        String mac = "";
        if (strcmp(topic, MQTT_RELAY_TOPIC) == 0) //Check if the received topic is the relay topic
        {
            if (checkMac(payload)) // Check if the mac received is the device mac
            {
                //Serial.println(payload[18]);
                if (payload[18] == '1') {
                    //Serial.println("Relay ON");
                    _bTarget_status = RELAY_ON;
                } else if (payload[18] == '0') {
                    //Serial.println("Relay OFF");
                    _bTarget_status = RELAY_OFF;
                }
            }
        }
    }
}

void relaySetupMQTT() {
    mqttRegister(relayMQTTCallback);
}

void _relayLoop() {

    unsigned long current_time = millis();

    switch (_RelayState) {
        case Init:
            if (_bTarget_status != _bCurrent_status) {
                _RelayState = Change;
            }
            break;
        case Change:
            digitalWrite(RELAY_PIN, _bTarget_status);
            _bCurrent_status = digitalRead(RELAY_PIN);
#if LED_RELAY_STATUS
            ledcommand(_bCurrent_status ? OFF : RED, 0);
#endif
#if MQTT_SUPPORT
            relayMQTT();
#endif
            _lFlood_start = millis();
            _RelayState = Wait;
            break;
        case Wait:
            if (TickGetDiff(millis(), _lFlood_start) > RELAY_FLOOD_TIME) {
                _RelayState = Init;
            }
            break;
        default:
            break;
    }
}

void relaySetup() {

    _relayConfigure();
    _relayBoot();

    relaySetupMQTT();

    // Main callbacks
    espurnaRegisterLoop(_relayLoop);
}