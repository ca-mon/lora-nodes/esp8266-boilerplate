// -----------------------------------------------------------------------------
// BUTTON
// -----------------------------------------------------------------------------
#include "headers/index.h"
#include "headers/main.h"
//Global variables for millis and button functions
bool _bCurrentButtonState;         // Current state of the button
long _millis_held;    // How long the button was held (milliseconds)
unsigned long _ulFirstTime; // how long since the button was first pressed 
byte _etatSignal = 0;


void buttonSetup() {
    pinMode(BUTTON_PIN, INPUT);

    // Main callbacks
    espurnaRegisterLoop(buttonLoop);
}

void buttonLoop() {
    _bCurrentButtonState = digitalRead(BUTTON_PIN);
    switch (_etatSignal) {
        case 0:
            if (_bCurrentButtonState == HIGH && TickGetDiff(millis(), _ulFirstTime) > 200) {
                _etatSignal++;
            }
            break;
        case 1:
            _ulFirstTime = millis();
            _etatSignal++;
            break;
        case 2:
            _millis_held = TickGetDiff(millis(), _ulFirstTime);
            if (_millis_held > 100) {
                // check if the button was released since we last checked
                if (_bCurrentButtonState == LOW) {
                    if (MS_TO_SEC(_millis_held) >= 20) {
                        wifiReset();
                        deferredReset(200, CUSTOM_RESET_HARDWARE);
                    } else if (MS_TO_SEC(_millis_held) >= 10 &&
                               MS_TO_SEC(_millis_held) <= 15) {    // If the button was held 10 secconds
                        deferredReset(200, CUSTOM_RESET_HARDWARE);
                    } else if (MS_TO_SEC(_millis_held) <= 3) { // Button pressed for less than 1 second
                        relaySwitch();
                        _ulFirstTime = millis();
                        _etatSignal = 0;
                    } else {
                        _ulFirstTime = millis();
                        _etatSignal = 0;
                    }
                }
                //else timeout error button
            }
            break;
        default:
            break;
    }
}
