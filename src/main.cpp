///////////////Libraries/////////////////////////////////////////////
#include <FS.h>                   //this needs to be first, or it all crashes and burns...
#include <vector>
#include "headers/index.h"

std::vector<void (*)()> _loop_callbacks;

///////////////objects/////////////////////////////////////////////

//Powermeter Object
//PowerMeter P1;

//TICK tReadingPM1= 0;
//TICK tReadingPM2 = 0;

///////////////Global_variables/////////////////////////////////////////////

// -----------------------------------------------------------------------------
// GENERAL CALLBACKS
// -----------------------------------------------------------------------------
void espurnaRegisterLoop(void(*callback)()) {
    _loop_callbacks.push_back(callback);
}

void setup() {

    Serial.begin(115200);

    systemSetup();
    ledSetup();
    Serial.println("Led setting OK");
    wifiSetup();
    Serial.println("Wifi setting OK");
    relaySetup();
    Serial.println("Relay setting OK");
    buttonSetup();
    Serial.println("Button setting OK");
    mqttSetup();
    Serial.println("MQTT setting OK");
    sensorSetup();
    Serial.println("Sensor setting OK");
    otamqttSetup();
    Serial.println("OTA setting OK");
    statusSetup();
    Serial.println("Status OK");
}

void loop() {
    // Call registered loop callbacks
    for (auto &_loop_callback : _loop_callbacks) {
        _loop_callback();
    }
}
