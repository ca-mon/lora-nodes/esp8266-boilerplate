#include "headers/PowerMeter.h"

// Constructor //
PowerMeter::PowerMeter() : Uart1(NULL) {}

bool PowerMeter::isFinished(void) {
    return ReadingState == finished;
}

bool PowerMeter::isSampling(void) {
    return (ReadingState == reading);
}

void PowerMeter::PowerMeterInit(byte rxPin, byte txPin) {
    if (NULL != Uart1) {
        delete Uart1;
    }
    Serial.begin(MCP39F501_BAUD_RATE);
    Serial.swap();
    Serial.write(0x5A); //Test if MCP39F501 connected
    Serial.flush();
    //Uart1 = new SoftwareSerial(rxPin, txPin);
    //Serial1.begin(MCP39F501_BAUD_RATE);
    pmDataLen = 0;
}

//Send frame to get power values
void PowerMeter::PowerMeterSend(void) {
    unsigned int i = 0;
    byte TxBuffer[50];

    TxBuffer[i++] = MCP_START_FRAME;    //Header byte
    TxBuffer[i++] = 0x08;                //Number of bytes
    TxBuffer[i++] = 0x41;                //Command Set Address Pointer
    TxBuffer[i++] = 0x00;                //Data byte Address0
    TxBuffer[i++] = 0x04;                //Data byte Address1 Current
    TxBuffer[i++] = MCP_READ;            //Command Register Read, N bits
    TxBuffer[i++] = 0x1A;                ////Number of Data bytes to read (26 bytes)
    TxBuffer[i++] = 0x5A;                //CheckSum = (0xA5 + 8 + 0x41 + 0x00 + 0x04 + 0x1A + 0x5A)% 0x100 or %256 in decimal

    Serial.flush();
    Serial.write(TxBuffer, i);
    ReadingState = reading;
}

//Clear serial buffer
void PowerMeter::PowerMeterClearBuffer(void) {
    pmDataLen = 0;
}

//get data size
byte PowerMeter::PowerMeterGetDataSize(void) {
    return (pmDataLen);
}

//Error management
void PowerMeter::error(byte e) {
    switch (e) {
        case 0 :
            DEBUG_WM("ACK not received");
            break;
        case 1 :
            DEBUG_WM("Incorrect number of byte received");
            break;
        case 2 :
            DEBUG_WM("No data received");
            break;
        default:
            break;
    }
}

//Enable power accumulation
void PowerMeter::StartEnergyCount() {
    bEnergycount = true;
}

//Disable power accumulation
void PowerMeter::StopEnergyCount() {
    bEnergycount = false;
}

//Get power values
void PowerMeter::ReadPowerValues(void) {
    byte byteCount = 0;
    byte RxData[50];
    byte Response[50];

    switch (ReadingState) {
        case reading:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 1) {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;

                if (pmData[0] == ACK)     //ACK received
                {
                    if (pmData[1] == NUMBER_OF_BYTES_READ) {
                        _PowerValues.fCurrent = CURRENT_MULTIPLIER * (uint32_t(pmData[2]) + uint32_t(pmData[3] << 8) +
                                                                      uint32_t(pmData[4] << 16) +
                                                                      uint32_t(pmData[5] << 24));
                        _PowerValues.fVoltage = VOLTAGE_MULTIPLIER * (uint16_t(pmData[6]) + uint16_t(pmData[7] << 8));
                        _PowerValues.fActivePower = ACTIVE_POWER_MULTIPLIER *
                                                    (uint32_t(pmData[8]) + uint32_t(pmData[9] << 8) +
                                                     uint32_t(pmData[10] << 16) + uint32_t(pmData[11] << 24));
                        _PowerValues.fReactivePower = REACTIVE_POWER_MULTIPLIER *
                                                      (uint32_t(pmData[12]) + uint32_t(pmData[13] << 8) +
                                                       uint32_t(pmData[14] << 16) + uint32_t(pmData[15] << 24));
                        _PowerValues.fApparentPower = APPARENT_POWER_MULTIPLIER *
                                                      (uint32_t(pmData[16]) + uint32_t(pmData[17] << 8) +
                                                       uint32_t(pmData[18] << 16) + uint32_t(pmData[19] << 24));
                        _PowerValues.fPowerFactor =
                                POWERFACTOR_MULTIPLIER * (int16_t(pmData[20]) + int16_t(pmData[21] << 8));
                        _PowerValues.fFrequency =
                                FREQUENCY_MULTIPLIER * (uint16_t(pmData[22]) + uint16_t(pmData[23] << 8));
                        //_PowerValues.ThermistorVoltage =  (uint16_t(pmData[24]) + uint16_t(pmData[25] << 8));

                        _PowerEvents.iEventsRegisters = uint16_t(pmData[26]) + uint16_t(pmData[27] << 8);


                        _PowerEvents.bOverCurrent = bitRead(_PowerEvents.iEventsRegisters, 15);
                        _PowerEvents.bOverVoltage = bitRead(_PowerEvents.iEventsRegisters, 14);
                        _PowerEvents.bOverPower = bitRead(_PowerEvents.iEventsRegisters, 13);
                        _PowerEvents.bUnderVoltage = bitRead(_PowerEvents.iEventsRegisters, 12);
                        _PowerEvents.bOverFrequency = bitRead(_PowerEvents.iEventsRegisters, 11);
                        _PowerEvents.bUnderFrequency = bitRead(_PowerEvents.iEventsRegisters, 10);
                        _PowerEvents.bOverTemperature = bitRead(_PowerEvents.iEventsRegisters, 9);
                        _PowerEvents.bUnderTemperature = bitRead(_PowerEvents.iEventsRegisters, 8);
                        _PowerEvents.bVoltageSag = bitRead(_PowerEvents.iEventsRegisters, 7);
                        _PowerEvents.bVoltageSurge = bitRead(_PowerEvents.iEventsRegisters, 6);

                        if (bEnergycount == true) {
                            if (_PowerValues.fApparentPower > APPARENT_POWER_THRESHOLD) {
                                _PowerValues.fPowerCount += _PowerValues.fApparentPower * SEC_TO_HR(
                                        MS_TO_SEC(TickGetDiff(millis(), TimeLastReading))) /
                                                            1000.0; //E(kWh) = P(W) � t(hr) / 1000
                            }
                        }
                        TimeLastReading = millis();
                        PowerMeterClearBuffer();
                    } else {
                        error(1);
                    }
                } else {
                    error(0);
                }
            } else {
                error(2);
            }
            ReadingState = finished;
            break;
        case finished:

            break;
        default:            //Reading command executed
            break;
    }
}

//Set range and calibrate frequency and gain
void PowerMeter::Calibration1Sequence(void) {
    uint8_t byteCount = 0;
    uint8_t RxData[50];
    uint8_t Response[3];

    switch (CalibrationState) {
        case 0:
            SetRange();
            CalibrationState++;
            break;
        case 1:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 1) {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;
            }
            if ((pmData[0] == ACK) && (pmDataLen == 1))     //just a single ACK
            {
                CalibrationState++;
            } else {
                CalibrationState = 9;       //calibration failed
                Response[0] = 0x28;         //Set range failed
            }
            break;
        case 2:
            FrequencyCalibrationCommand();
            CalibrationState++;
            break;
        case 3:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 0)  //response received after the calibration command
            {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;
            }
            if ((pmData[0] == ACK) && (pmDataLen == 1))     //just a single ACK
            {
                CalibrationState++;
            } else {
                CalibrationState = 9;       //calibration failed
                Response[0] = 0x29;         //Frequency calibration failed
            }
            break;
        case 4:
            GainCalibrationCommand();
            CalibrationState++;
            break;
        case 5:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 0)  //response received after the calibration command
            {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;
            }
            if ((pmData[0] == ACK) && (pmDataLen == 1))     //just a single ACK
            {
                CalibrationState++;
            } else {
                CalibrationState = 9;       //calibration failed
                Response[0] = 0x2A;         //Gain voltage calibration failed
            }
            break;
        case 6:
            SaveToFlashCommand();
            CalibrationState++;
            break;
        case 7:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 0)  //response received after the calibration command
            {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;
            }
            if ((pmData[0] == ACK) && (pmDataLen == 1))     //just a single ACK
            {
                CalibrationState++;
            } else {
                CalibrationState = 9;       //calibration failed
                Response[0] = 0x2D;         //Save to flash failed
            }
            break;
        case 8:
            Response[0] = 0x20;               //Calibration successful
        case 9:
            //RN4020SendData(Response, 1);     //Calibration failed
        default:
            CalibrationState = 0;
            Calibration1 = false;
            break;
    }
}

//Set range and calibrate reactive
void PowerMeter::Calibration2Sequence(void) {
    uint8_t byteCount = 0;
    uint8_t RxData[50];
    uint8_t Response[3];

    switch (CalibrationState) {
        case 0:
            ReactiveCalibrationCommand();
            CalibrationState++;
            break;
        case 1:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 0)  //response received after the calibration command
            {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;
            }
            if ((pmData[0] == ACK) && (pmDataLen == 1))     //just a single ACK
            {
                CalibrationState++;
            } else {
                CalibrationState = 5;       //calibration failed
                //here the error code must be sent back to Android
                //...
                Response[0] = 0x2F;           //Gain reactive power calibration failed
            }
            break;
        case 2:
            SaveToFlashCommand();
            CalibrationState++;
            break;
        case 3:
            byteCount = Serial.readBytes(RxData, NUMBER_OF_BYTES_READ); //read 29 bytes on the serial buffer
            if (byteCount > 0)  //response received after the calibration command
            {
                memcpy(pmData, RxData, byteCount);
                pmDataLen = byteCount;
            }
            if ((pmData[0] == ACK) && (pmDataLen == 1))     //just a single ACK
            {
                CalibrationState++;
            } else {
                CalibrationState = 5;       //calibration failed
                //here the error code must be sent back to Android device
                //...
                Response[0] = 0x2D;           //Save to flash failed
            }
            break;
        case 4:
            Response[0] = 0x20;               //Calibration successful
        case 5:
            //RN4020SendData(Response, 1);     //Calibration failed
        default:
            CalibrationState = 0;
            Calibration2 = false;
            break;
    }
}

//Set Range
void PowerMeter::SetRange(void) {
    uint16_t i = 0;
    uint8_t TxBuffer[50];

    TxBuffer[i++] = MCP_START_FRAME;    //Header byte
    TxBuffer[i++] = 0x1C;                //Number of bytes
    TxBuffer[i++] = 0x41;                //Command Set Address Pointer
    TxBuffer[i++] = 0x00;                //Data byte Address0
    TxBuffer[i++] = 0x48;                //Data byte Address1 Range
    TxBuffer[i++] = MCP_WRITE;            //Command Register Write, N bits
    TxBuffer[i++] = 0x14;                //Number of Data bytes to write (20 bytes)
    TxBuffer[i++] = 0x12;                //Range 1 Voltage bit 0 to bit 7
    TxBuffer[i++] = 0x0C;                //Range 2 Current bit 8 to bit 15
    TxBuffer[i++] = 0x14;                //Range 3 Power bit 16 to bit 23
    TxBuffer[i++] = 0x0;                //Range 4 Non-used so 0
    TxBuffer[i++] = 0x50;                //Calibration Current 1 bit 0 to bit 7
    TxBuffer[i++] = 0xC3;                //Calibration Current 2	bit 8 to bit 15
    TxBuffer[i++] = 0x0;                //Calibration Current 3 bit 16 to bit 23
    TxBuffer[i++] = 0x0;                //Calibration Current 4 bit 24 to bit 31
    TxBuffer[i++] = 0x98;                //Calibration Voltage 1 bit 0 to bit 7
    TxBuffer[i++] = 0x08;                //Calibration Voltage 2 bit 8 to bit 15
    TxBuffer[i++] = 0xB0;                //Calibration Power Active 1
    TxBuffer[i++] = 0xAD;                //Calibration Power Active 2
    TxBuffer[i++] = 0x01;                //Calibration Power Active 3
    TxBuffer[i++] = 0x0;                //Calibration Power Active 4
    TxBuffer[i++] = 0x24;                //Calibration Power Reactive 1
    TxBuffer[i++] = 0x74;                //Calibration Power Reactive 2
    TxBuffer[i++] = 0x01;                //Calibration Power Reactive 3
    TxBuffer[i++] = 0x0;                //Calibration Power Reactive 4
    TxBuffer[i++] = 0x50;                //Line Frequency Reference 1
    TxBuffer[i++] = 0xC3;                //Line Frequency Reference 2
    TxBuffer[i++] = 0xD4;                //CheckSum

    Serial.write(TxBuffer, i);
}

//Do frequency calibration
void PowerMeter::FrequencyCalibrationCommand(void) {
    unsigned int i = 0;
    uint8_t TxBuffer[50];

    TxBuffer[i++] = MCP_START_FRAME;    //Header byte
    TxBuffer[i++] = 0x04;                //Number of bytes
    TxBuffer[i++] = 0x76;                //Auto-Calibration Frequency command
    TxBuffer[i++] = 0x1F;                //CheckSum

    Serial.write(TxBuffer, i);
}

//Do gain calibration
void PowerMeter::GainCalibrationCommand(void) {
    unsigned int i = 0;
    uint8_t TxBuffer[50];

    TxBuffer[i++] = MCP_START_FRAME;    //Header byte
    TxBuffer[i++] = 0x04;                //Number of bytes
    TxBuffer[i++] = 0x5A;                //Auto-Calibrate Gain command
    TxBuffer[i++] = 0x03;                //CheckSum

    Serial.write(TxBuffer, i);
}

//Do reactive calibration
void PowerMeter::ReactiveCalibrationCommand(void) {
    unsigned int i = 0;
    uint8_t TxBuffer[50];

    TxBuffer[i++] = MCP_START_FRAME;    //Header byte
    TxBuffer[i++] = 0x04;                //Number of bytes
    TxBuffer[i++] = 0x7A;                //Auto-Calibrate Reactive Gain command
    TxBuffer[i++] = 0x23;                //CheckSum

    Serial.write(TxBuffer, i);
}

//Save data to flash
void PowerMeter::SaveToFlashCommand(void) {
    unsigned int i = 0;
    uint8_t TxBuffer[50];

    TxBuffer[i++] = MCP_START_FRAME;    //Header byte
    TxBuffer[i++] = 0x04;                //Number of bytes
    TxBuffer[i++] = 0x53;                //Save To Flash command
    TxBuffer[i++] = 0xFC;                //CheckSum

    Serial.write(TxBuffer, i);
}

void PowerMeter::DEBUG_WM(String text) {
    if (_debug) {
        Serial.print("*PM: ");
        Serial.println(text);
    }
}

void PowerMeter::setDebugOutput(bool debug) {
    _debug = debug;
}