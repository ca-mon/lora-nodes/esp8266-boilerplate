// -----------------------------------------------------------------------------
// WIFI
// -----------------------------------------------------------------------------
#include <ESP8266WiFi.h>            //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>            //https://github.com/bblanchon/ArduinoJson
#include "headers/index.h"
#include "headers/main.h"

WiFiEventHandler _wifiDisconnectHandler;
WiFiManager _wifiManager;

//flag for saving data
bool _shouldSaveConfig = false;
bool _bReconnect = false;
//define your default values here, if there are different values in config.json, they are overwritten.
char _mqtt_server_ip[40];
char _socket_name[34] = "Prise salon";

// -----------------------------------------------------------------------------
// PRIVATE
// -----------------------------------------------------------------------------

//callback notifying us of the need to save config
void _saveConfigCallback() {
    //Serial.println("Should save config");
    _shouldSaveConfig = true;
}

//Function called when AP mode and config portal are started
void _onAPstarted(WiFiManager *) {
    ledcommand(BLUE, 0.2);
}

void _onWifiDisconnect(const WiFiEventStationModeDisconnected &event) {
    //Serial.println("Disconnected from Wi-Fi.event detected");
    //mqttReconnectTimer.detach(); // ensure we don't reconnect to MQTT while reconnecting to Wi-Fi
    _bReconnect = true;
}

//Function for connecting to wifi
void _connectToWifi() {

    ledcommand(ORANGE, 0.2);

    //fetches ssid and pass and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    if (!_wifiManager.autoConnect(("AutoConnect_Houseone" + String(ESP.getChipId())).c_str(), "password")) {
        Serial.println("failed to connect and hit timeout");
        delay(3000);
        //reset and try again, or maybe put it to deep sleep
        ESP.reset();
        delay(5000);
    }

    _onWifiConnect();
}

// Function for connecting to mqtt server  WiFiManager *myWiFiManager
void _onWifiConnect() {
    /*Serial.println("Connected to Wi-Fi.");
    Serial.println("local ip");
    Serial.println(WiFi.localIP());*/
    ledcommand(ORANGE, 0);
}

char *getServerIP() {
    return _mqtt_server_ip;
}

String getSocketName() {
    return _socket_name;
}

void wifiReset() {
    _wifiManager.resetSettings();
}

void wifiSetup() {
    //clean FS, for testing
    //SPIFFS.format();

    //read configuration from FS json
    //Serial.println("mounting FS...");
    if (SPIFFS.begin()) {
        //Serial.println("mounted file system");
        if (SPIFFS.exists("/config.json")) {
            //file exists, reading and loading
            //Serial.println("reading config file");
            File configFile = SPIFFS.open("/config.json", "r");
            if (configFile) {
                //Serial.println("opened config file");
                size_t size = configFile.size();
                // Allocate a buffer to store contents of the file.
                std::unique_ptr<char[]> buf(new char[size]);

                configFile.readBytes(buf.get(), size);
                DynamicJsonBuffer jsonBuffer;
                JsonObject &json = jsonBuffer.parseObject(buf.get());
                //json.printTo(Serial);
                if (json.success()) {
                    //Serial.println("\nparsed json");

                    strcpy(_mqtt_server_ip, json["mqtt_server"]);
                    //strcpy(mqtt_port, json["mqtt_port"]);
                    strcpy(_socket_name, json["socket_name"]);

                } else {
                    //Serial.println("failed to load json config");
                }
                configFile.close();
            }
        }
    } else {
        Serial.println("failed to mount FS");
    }
    //end read

    // The extra parameters to be configured (can be either global or just in the setup)
    // After connecting, parameter.getValue() will get you the configured value
    // id/name placeholder/prompt default length
    _wifiManager.setDebugOutput(false);
    WiFiManagerParameter custom_mqtt_server_ip("server", "mqtt server", _mqtt_server_ip, 40);
    //WiFiManagerParameter custom_mqtt_port("port", "mqtt port", mqtt_port, 6);
    WiFiManagerParameter custom_socket_name("name", "socket_name", _socket_name, 32);

    //Callback  onWifiConnect function when the esp is connected to the wifi
    //wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
    _wifiDisconnectHandler = WiFi.onStationModeDisconnected(_onWifiDisconnect);

    //set config save notify callback
    _wifiManager.setSaveConfigCallback(_saveConfigCallback);
    //wifiManager.setSTACallback(onWifiConnect);
    _wifiManager.setAPCallback(_onAPstarted);

    //set custom html head elment
    _wifiManager.setCustomHeadElement(
            "<div><img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAjCAYAAAAe2bNZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAFHGlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDIgNzkuMTYwOTI0LCAyMDE3LzA3LzEzLTAxOjA2OjM5ICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxOCAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE4LTEwLTI5VDE1OjE0OjA5KzAxOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxOC0xMC0yOVQxNToxNzoyMSswMTowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOC0xMC0yOVQxNToxNzoyMSswMTowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5MmUxN2FlZi1hNGRkLTA1NGYtOGViMC05YTYwMzI2ZTliYTEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OTJlMTdhZWYtYTRkZC0wNTRmLThlYjAtOWE2MDMyNmU5YmExIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6OTJlMTdhZWYtYTRkZC0wNTRmLThlYjAtOWE2MDMyNmU5YmExIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo5MmUxN2FlZi1hNGRkLTA1NGYtOGViMC05YTYwMzI2ZTliYTEiIHN0RXZ0OndoZW49IjIwMTgtMTAtMjlUMTU6MTQ6MDkrMDE6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE4IChXaW5kb3dzKSIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6vv0HGAAAKf0lEQVRYhZVYW2xcxRn+Zubsei9nd+31bdd3r53EYUliEjByEpRQO1CggFREW6BRkdqqLeolT1StRNXbQ2+oCJSqgpcCUoNUiSqQioZSIHViJwEndq4uia+JE9vr9Xpv535m+rC75qzjBDovZ+acmX+++f9v/ssh/f27CQAKQAAgxSdu0f+sMSk+byZnrW8EgJCKQKhjEvm8AoUQkCRJMMZs0zQp55wSQpwb/D9geAmEc0Kpf6sx51zA4/FwIbidzeZQUeHmbrfbEkLcaq0AwFFuhZW51PHCeRqnMKz+xjknsuyzCAE/dWr0qWPHjg+PjJx9hjEGj8djCSHIzdbiU82v1hZYLNZGcKNpbiYMnHPIsmxZlo3BwZP7lpZSLwshoplM9qGlpZRobIwc8Xo9XNd1RghZLeumckuaWT2pbIJzLIRAICBbhmFgYGDwN6nU8h8BIByuegMAFheTvzh27MSfVFVDMBg0hRBOLjpl3QCkBOZWpnHgKABRVRVHjhx7NZvN/RgAurrWP9Xfv/uJ9es7HwSAdDrzvYGBoTczmSyCwaC5BojVB14BtpaZnBMIAHAuSCgUtDKZDBsYOH5Y07QvA8DGjRvui8e7Dqqqimi0/rLb7To8P5940rKszVevXrs/FAocqK4O64ZhMiEAQtbUyAqHKG5sZYQTQqCyMmilUqnqgYGhU7qu7wGQ3LTptu54vOtf2WyOapruyucVtm5dx/G77rpjMyFkxjTN3sHBk6PT01eaAwHZZIyINYhd1i+Z6abkrawMWYlEsmNgYGjUNM3NAC53d2/q3rBh3Wg6nWG2bTNCiOCck3Q6I7W0NI9v397TLUlsFEDn8PDI6Pj45OZAIGC7XBIv3Pw1b5UomcnpgYkQApRSOxQK8StXZncODZ0cEkKEAQxu29a9IxZrS2YyWYlzQYs3ZmWtpmk0HK5S6uvrXp6bW9huWVZ8fn7hu7ZtDzY0RMeFALdtWyr4xpVG1+QM5xxer9cKBGRcujT+2KlTo/8E4ALwZk/Pnfe3tjaZ6XRGAkCKHCgzKyGE6LpOg8GA3dAQeT2ZTHVomr4lmVzaqyjKTGNjwwhjjFuWRYuAVjTkBAMhBILBgKWqKi5cGHtmbOyTVwGgqiq0v7t789NNTQ0oAlmLXyXBIIRA0wzq83l5U1Pj31OpZZ+iqDvS6cyjqdSy0dzccNTlcnHTNJlTQytmEkIgFApaS0spDA2d/FUikfwtAHR0tP+kp2fbT71eL7LZnMux+GaedUVDhmEyl8vFW1qa3svn1VQmk30gn1f6FhYS1Q0NkXf8fh/XdWMlnq2ACYVC1sJCAseOnXjFNK19ALBly+3fiMc37lcUFbquS58BxNlWNGSaJiWE8La25hMAObO4mPyqpul3z80txKurw38LhULcMAwGQLD29lYSCMj23Nw8BgdPvi2E+FpFRcVMPN61a/36zncVRYVhGDdohBBCKKWCEMIppaCUruXQQAiBbdtMCM6j0cgYIeS1dDp7v6pqu65evXZvTU34jWAwaBiGQVk8vlFcuzbXcPLk8AkAOwDA5XItqapWd/HiJ1/3eCqGw+HKlGXZRAgBt9tte70ezpjEKaWCUgLOhQCEcLvdwuOp4IUrDM55ISvw+3321NSV9o8+OvWyruu9lmU12zav4Zy3XblydW91ddXbsuxPSpIkQVHUKICNpdNomtauado3ASCRWDzS3Ny03+Mh3OOpwOJiEonEYlMyudSTy+XvsCw7attWhRBC93g887LsHw2Hq06Ew1XTsuy3TNMCIQSZTOYhVVUfV1W1zJ6ci9aFhURLfX3duKQoCm1ubhymlNypqlrE5XJxSimfmpp+TlHUHS6XpAWDAYyPT2B5Of3YxMTUXk3TH8UaLZvNIZFYxOTkNPx+3zv19XWvt7Q0HWhpaQJjLA8Asiwfb2lp+rlpmpRSyiili+Fw1XFFUSnp69vFinmITQgBYxSUUhw8+I9BWZbN/v7duy5fnug9ffrM7wDsdOy94PP5Rn0+7zghJGdZpt80rXZFUTZzLhpKkyRJ+vj227uebW5u/uDo0aFDuVw+9MgjD9wjhIBt20U3oDPDMED6+3dTAKxITEEpFaZpsvn5hf5YrP2diYnJ71+48N+XSsIDAfnV1tbmv9TX133o9Xrh9/tQEmzbNrLZHBYWEttnZ68/vbSU+nZpXUtL07ONjQ2/T6fT90SjkROMMavEqZLFnGA4Pg0HoqmpwTp69Ph3xscn/1wk9Qc9PVv3RaORM4QQ5HJ5LC4mI5lM5jaABAlB1uv1jNXW1s7W1FTDsixcvjyx/vz5sed1Xf8SADQ2Rp/bs+feX8/OXqecc+YIJdQJZgUhIYRzLqR0Ol0lhNDOnDk3AJBz9933hSeDwQASiUXX2NilH16/Pve4bdt3r+YNY2y4ujr8Zmdn7KXOzlg2lVrGkSNH/2ya1s7e3rt2K4pqeL0eTZIkuxg0S87yRjCMMQsAfe+9Dz9samo82N7e+rwkMQQCMgYHTzw8P5/YD6DZsX9SkqS0ZVkBALWO94lYrPVH8fhtB0zTgNvtxrlzY9+anp75Sl/fri8SQrht22WhRXIgEyjkL0SSGJdlv5ienvlDKBR8f+vWzacPHTq8d3Ex+VpxYaq2tubFtraWt3w+7ymv1wtN05DJ5DbNz88/PDt7/QcAIhMT03/VdaO2r2/3iyMjZ+MTE5Ov+P2+QUopL+Y2xEEPSCgvI0jBY3IAmAGAdHp5Zy6XP93ZGft4eXn5is/nuxaPb3ywoSGyRCnF8nIa+XweXq8XsVjr2a6udWdnZq6+MDx8+i3Lsrs6Otr/oygqkslkHwBUVFRMEELAOXfWTCtFHBwvUcxlAMANAG63J20YBmprqy/29vZ0ut1uIxqNYHJyqnV09Nw+RVH3cM7rGWNJWfb/u7297YXOztil3t6efs55hSzLuq7r8Hg8y0X5bseeZa0EpqxcJYSAUgagkN8QQpHP56RAIGB4PBUYGTnz0LlzFw85Bdm2XZNOZzaMjJx5JpvNPrF165Y38vm8rqqqS5b9ZvGA4JyjSFxnEyUwa0VcARRW2LYFQIBSKgzDkAghts/nnQKwBIDHYm2/9Ho951VVWzcxMfUzxlgoEqm/lM8rxDBMRikVQgiYZqFQEIWGVYGXlszkrChp0Z6wbbtQPjBWEkIAQNM0Fo1Gzvf0bOswTZPGYm1Lts1BCN6vr689IEkuV2VlKJnPKy5KSWFnrGR0YIwRSils2y6jh9NMTlPxYtivBQDLsoIlFZdMmMvlpdrammVKSVnmV10dznAuoCgFIKXNKCUQQgSKh6ktWuWGcnp1RSk458zlcpFgMHiYMfZJJFJ33DStskWEEOi6LqmqVuYnNE2XiqYsy21sm6OyMjQEkMt1dbXvUsrgqDY/PWh//25WBLUSDhhjwjRNy7JsBAJ+aJpe2uBz1eMov7K8gJ9aipJHIBAA57bEuXD+haAo/hIpnaJUQwnLsuB2uyHLfmiaXkqanaXw6gqUrho7HSkp8E1IlZWVRT5y5+1dqU5KBOZl6iJEWJbFiv3SSW9Zi+PG5nQVQggBXddpYUj4qrk2APE/QJ+HLdA0BZQAAAAASUVORK5CYII=' /></div>");

    //set static ip
    //wifiManager.setSTAStaticIPConfig(IPAddress(10,0,1,99), IPAddress(10,0,1,1), IPAddress(255,255,255,0));

    //add all your parameters here
    _wifiManager.addParameter(&custom_mqtt_server_ip);
    //wifiManager.addParameter(&custom_mqtt_port);
    _wifiManager.addParameter(&custom_socket_name);

    //set minimu quality of signal so it ignores AP's under that quality
    //defaults to 8%
    //wifiManager.setMinimumSignalQuality();

    //sets timeout until configuration portal gets turned off
    //useful to make it all retry or go to sleep
    //in seconds
    //wifiManager.setTimeout(120);

    _connectToWifi();

    //read updated parameters
    strcpy(_mqtt_server_ip, custom_mqtt_server_ip.getValue());
    //strcpy(mqtt_port, custom_mqtt_port.getValue());
    strcpy(_socket_name, custom_socket_name.getValue());

    //save the custom parameters to FS
    if (_shouldSaveConfig) {
        Serial.println("saving config");
        DynamicJsonBuffer jsonBuffer;
        JsonObject &json = jsonBuffer.createObject();
        json["mqtt_server"] = _mqtt_server_ip;
        //json["mqtt_port"] = mqtt_port;
        json["socket_name"] = _socket_name;

        File configFile = SPIFFS.open("/config.json", "w");
        if (!configFile) {
            Serial.println("failed to open config file for writing");
        }

        json.printTo(Serial);
        json.printTo(configFile);
        configFile.close();
        //end save
    }

    espurnaRegisterLoop(wifiLoop);
}

void wifiLoop() {
    if (_bReconnect) {
        _bReconnect = false;
        _connectToWifi();
    }
}
