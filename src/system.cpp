#include <Ticker.h>
#include "headers/index.h"
#include "headers/main.h"

Ticker _defer_reset;

uint8_t _reset_reason = 0;
// -----------------------------------------------------------------------------
// Reset
// -----------------------------------------------------------------------------

void resetReason(unsigned char reason) {
    _reset_reason = reason;
}

void reset() {
    ESP.restart();
}

void deferredReset(unsigned long delay, unsigned char reason) {
    _defer_reset.once_ms(delay, resetReason, reason);
}

bool checkNeedsReset() {
    return _reset_reason > 0;
}

void systemSetup() {

    // Question system stability
#if SYSTEM_CHECK_ENABLED
    systemCheck(false);
#endif

    // Register Loop
    espurnaRegisterLoop(systemLoop);
}

void systemLoop() {

    // -------------------------------------------------------------------------
    // User requested reset
    // -------------------------------------------------------------------------

    if (checkNeedsReset()) {
        reset();
    }
}